/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/31 17:37:09 by vbudnik           #+#    #+#             */
/*   Updated: 2018/04/19 21:18:07 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include <fcntl.h>
# include "../libft/libft.h"
# include <sys/stat.h>
# include "../minilibx_macos/mlx.h"

# define HEIGHT 1200
# define WIDTH 1000

typedef struct		s_ptr
{
	void			*mlx;
	void			*win;
	void			*image;
}					t_ptr;

typedef struct		s_values
{
	int				x;
	int				y;
	int				z;
	double			absc;
	double			ord;
	int				nexta;
	int				nexto;
	int				zoomx;
	int				zoomy;
	int				center;
	int				fd;
}					t_values;

typedef struct		s_all
{
	t_ptr			ptr;
	t_values		value;
	int				**map;
}					t_all;

int					get_values(char *path, t_values *values);
int					**get_map(char *path, int y, int x, int fd);
void				calc_values(t_values *val, int z, int a, int o);
void				display(t_values v, int **map, t_ptr *ptr);
void				draw_line(t_ptr *ptr, t_values v, int nextx, int nexty);
void				draw_absc(t_values v, int o, int z, t_ptr *ptr);
void				draw_ord(t_values v, int o, int z, t_ptr *ptr);
void				valid(int ac, char **av);
void				valid_item(char *tmp);
void				valid_size(char **tmp, int x);
void				ft_exit_mas(char *mass);
void				exit_hex(char tmp);

#endif
