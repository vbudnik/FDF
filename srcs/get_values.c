/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_values.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/25 14:12:32 by vbudnik           #+#    #+#             */
/*   Updated: 2018/04/19 21:07:36 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

void	calcul_x_y(char *path, t_values *val)
{
	char		*line;
	char		**tmp;
	int			flag;

	flag = 0;
	val->fd = open(path, O_RDONLY);
	while (get_next_line(val->fd, &line) && line)
	{
		if (line && flag == 0)
		{
			flag = 1;
			tmp = ft_strsplit(line, ' ');
			while (tmp[val->x] != '\0')
			{
				free(tmp[val->x]);
				val->x++;
			}
			free(tmp);
		}
		ft_strdel(&line);
		free(line);
		val->y++;
	}
	free(line);
	close(val->fd);
}

int		get_values(char *path, t_values *values)
{
	values->x = 0;
	values->y = 0;
	calcul_x_y(path, values);
	values->zoomx = 30;
	values->zoomy = 20;
	values->center = 300;
	if (values->x == 0 && values->y == 0)
		return (0);
	return (1);
}

void	del_me(char *line, char **tmp)
{
	ft_strdel(&line);
	free(tmp);
}

int		**get_map(char *path, int y, int x, int fd)
{
	char		*line;
	int			i;
	int			j;
	int			**map;
	char		**tmp;

	j = -1;
	map = (int **)malloc(sizeof(int *) * y);
	fd = open(path, O_RDONLY);
	while (++j < y && get_next_line(fd, &line))
	{
		tmp = ft_strsplit(line, ' ');
		i = -1;
		valid_size(tmp, x);
		map[j] = (int *)malloc(sizeof(int) * x);
		while (++i < x)
		{
			valid_item(tmp[i]);
			map[j][i] = ft_atoi(tmp[i]);
			ft_strdel(&tmp[i]);
		}
		del_me(line, tmp);
	}
	close(fd);
	return (map);
}

void	calc_values(t_values *val, int z, int a, int o)
{
	val->absc = (a * val->zoomx) + val->center;
	val->ord = ((o - z) * val->zoomy) + val->center;
}
