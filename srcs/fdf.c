/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/25 14:14:49 by vbudnik           #+#    #+#             */
/*   Updated: 2018/04/19 20:52:50 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

void	init_ptr(t_ptr *ptr, char *path)
{
	ptr->mlx = mlx_init();
	ptr->win = mlx_new_window(ptr->mlx, HEIGHT, WIDTH, path);
	ptr->image = mlx_new_image(ptr->mlx, HEIGHT, WIDTH);
}

int		ft_key(int key, t_all *d)
{
	if (key == 53)
	{
		system("leaks fdf");
		exit(0);
	}
	else if (key == 123)
	{
		d->value.zoomx -= 3;
		d->value.zoomy -= 2;
	}
	else if (key == 124)
	{
		d->value.zoomx += 3;
		d->value.zoomy += 2;
	}
	else if (key == 126)
		d->value.center += 100;
	else if (key == 125)
		d->value.center -= 100;
	mlx_clear_window(d->ptr.mlx, d->ptr.win);
	display(d->value, d->map, &d->ptr);
	return (0);
}

int		exit_x(void *win_ptr)
{
	win_ptr = NULL;
	exit(1);
	return (0);
}

void	display(t_values v, int **map, t_ptr *ptr)
{
	int				o;
	int				a;

	o = 0;
	while (o < v.y)
	{
		a = 0;
		while (a < v.x)
		{
			calc_values(&v, map[o][a], a, o);
			if (v.absc < HEIGHT && v.ord < WIDTH)
			{
				mlx_pixel_put(ptr->mlx, ptr->win, v.absc, v.ord, 0x00FF00);
				if (a + 1 < v.x)
					draw_absc(v, o, map[o][a + 1], ptr);
				if (o + 1 < v.y)
					draw_ord(v, o, map[o + 1][a], ptr);
			}
			a++;
		}
		o++;
	}
}

int		main(int ac, char **av)
{
	t_all			datas;
	int				fd;

	valid(ac, av);
	if (get_values(av[1], &datas.value))
	{
		datas.map = get_map(av[1], datas.value.y, datas.value.x, fd);
		init_ptr(&datas.ptr, av[1]);
		datas.value.nexta = 0;
		datas.value.nexto = 0;
		display(datas.value, datas.map, &datas.ptr);
		mlx_key_hook(datas.ptr.win, ft_key, &datas);
		mlx_hook(datas.ptr.win, 17, 0, exit_x, datas.ptr.mlx);
		mlx_loop(datas.ptr.mlx);
	}
	return (0);
}
