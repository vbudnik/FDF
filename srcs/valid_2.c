/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   valid_2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/14 15:59:32 by vbudnik           #+#    #+#             */
/*   Updated: 2018/04/14 16:06:58 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

void	ft_exit_mas(char *mass)
{
	ft_putendl(mass);
	exit(0);
}

void	exit_hex(char tmp)
{
	if (!(ft_ishex(tmp)))
	{
		ft_putendl("Format of color not valid");
		exit(0);
	}
}
